CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The AJAX Screen Lock module simply locks the screen when AJAX is running on the
page. The user can prevent unwanted user actions while the AJAX request is being
performed. A popup appears automatically when the AJAX query is sent.

Popup window is based on the jquery.blockUI.js plugin.

 * For a full description of the module visit:
   https://www.drupal.org/project/ajax_screen_lock

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/ajax_screen_lock


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

 * jQuery BlockUI library - https://github.com/malsup/blockui


INSTALLATION
------------

 * Install the AJAX Screen Lock module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > User interface > AJAX Screen
       Lock to configure the module. Save configuration.


MAINTAINERS
-----------

 * Eugene Ilyin (eugene.ilyin) - https://www.drupal.org/u/eugeneilyin
 * Ana Colautti (anacolautti) - https://www.drupal.org/u/anacolautti

Supporting organization:

 * DrupalJedi - https://www.drupal.org/drupaljedi
 * Easytechgreen - https://www.drupal.org/easytechgreen
